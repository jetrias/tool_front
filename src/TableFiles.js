import Table from 'react-bootstrap/Table';

function TableFiles(props) {
    const { files } = props;

    const rederChildTable = (fileName, lines) => {
        if (lines.length > 0) {
            return lines.map((line) => {
                return (
                    <tr key={line.hex}>
                        <td>{fileName}</td>
                        <td>{line.text}</td>
                        <td>{line.number}</td>
                        <td>{line.hex}</td>
                    </tr>
                )
            })
        }
    }

    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>File Name</th>
                    <th>Text</th>
                    <th>Number</th>
                    <th>Hex</th>
                </tr>
            </thead>
            <tbody>
                {
                    // map files obj
                    (files.length > 0) &&
                    files.map((value) => {
                        return rederChildTable(value.file, value.lines)
                    })
                }
            </tbody>
        </Table>
    );
}

export default TableFiles;