# Descripcion
Este es un front disenapo para consumir un API y mostrarlos.

Este api esta desarrollado con React y React-bootstrap

# 1. Uso del proyecto
## Instalación
Una vez que hayas descargado el proyecto del repositorio te ubicas en la carpeta del proyecto y ejecutas: 
```
npm install
```
## Ejecución
Una vez se hayan instalado todas las dependencias puedes ejecutar el proyecto usando el siguiente comando
```
npm start
```

## Configuración
El proyecto esta configurado para correr en la siguiente dirección y puerto: 
```
http://localhost:3000/
```

## Composición
Consta de una vista que al arrancar el proyecto muestra la informacion obtenida del API como se detalla en la siguiente imagen.
![vista](/public/view.png)

Se desarrollo usando el diagrama de secuencias que se observa a continuacion en la seccion Frontend React y su interaccion con el API.
![vista](/public/diagrama.png)

### Estructura del proyecto
![vista](/public/estructura_v.png)
En el directorio public se encuentran las imagenes, el archivo **index.html** que es la plantilla inicial el **manifest.json** donde esta la configuracion inicial de React para decirle al navegador que informacion debe mostar. Tambien esta el archivo **robots.txt** el cual sirve para indicar a los bots de busqueda cual es la estructura de la pagina.

Luego tienes el directorio **src** que contiene el directorio **lib** y ahi se encuentra el archivo **getData.js** el cual es el responsable de hacer la peticion al API via axios.

En la raiz de src tienes tambien los archivos:
- **App.js** el cual define el comportamiento de la aplicacion.
- **App.test.js** donde se definen las pruebas unitarias.
- **index.css** define el estilo usara la aplicacion.
- **index.js** es donde se hace el renderizado de los componentes.
- **setupTests.js** aqui se deben configurar las pruebas.
- **TableFiles.js** este es el componente de la tabla html que se muestra en la vista.
- **package-lock.json** donde se encuantran todas las dependencias especificadas.
- **package.json** donde se encuentra la información general del proyecto.

## Librerias usadas
- axios:1.2.2
- bootstrap: 5.2.3
- react: 18.2.0
- react-bootstrap: 2.7.0
- react-dom: 18.2.0
- react-scripts: 5.0.1

#### El repositorio de este proyecto es: <https://gitlab.com/jetrias/tool_front.git>
#### El API que utiliza este proyecto lo consigues en: <https://gitlab.com/jetrias/tool.git> 