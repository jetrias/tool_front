import { useEffect, useState } from 'react';
import './App.css';
import TableFiles from './TableFiles';
import getData from './lib/getData';


function App() {
  const [searchInput, setSearchInput] = useState('');
  const [testData, setTestData] = useState([]);

  useEffect(() => {
    getData(searchInput).then((res)=>{
      console.log(res.data.data)
      const data = res.data.data;
      setTestData(data);
    }).catch((e)=>{console.log(e)})
  }, [searchInput])


  return (
    <div className='cont-table-and-header'>
      <div className='header-alert'>
        React Test App
      </div>
      <TableFiles files={testData} />
    </div>
  );
}

export default App;
